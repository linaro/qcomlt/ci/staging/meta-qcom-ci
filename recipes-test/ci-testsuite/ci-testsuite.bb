SUMMARY = "QComLT CI Testsuite"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

inherit systemd

SRC_URI = "git://git.codelinaro.org/linaro/qcomlt/ci/staging/ci-testsuite.git;protocol=https;branch=master"
SRCREV = "221326f7bd234f3c2fdb7492eb9276b250f1957b"

S = "${WORKDIR}/git"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "ci-testsuite.service"

do_compile () {
}

do_install () {
	oe_runmake install SYSTEMD_PREFIX=${D}${systemd_system_unitdir} PREFIX=${D}${exec_prefix}
}

do_configure () {
}

FILES:${PN} += "/usr/lib/systemd/system-shutdown"
