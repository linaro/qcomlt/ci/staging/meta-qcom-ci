require recipes-test/images/initramfs-test-image.bb

PACKAGE_INSTALL += " \
	fastrpc \
	pd-mapper \
	qrtr \
	rmtfs \
	tqftpserv \
	sysbench \
	util-linux \
	ci-testsuite \
"

# Remove until it fails in create-spdx
PACKAGE_INSTALL:remove = "read-edid"
